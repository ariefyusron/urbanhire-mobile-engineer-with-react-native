import React, { Component } from 'react';
import DatePicker from 'react-native-datepicker';

class DateInput extends Component {

  render() {
    const { input, date } = this.props

    return (
      <DatePicker
        style={{width: '55%', alignSelf: 'center'}}
        date={input.value || date}
        mode="date"
        placeholder="select date"
        format="YYYY-MM-DD"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            right: 0
          },
          dateInput: {
            borderWidth: 0,
          }
        }}
        onDateChange={input.onChange}
      />
    );
  }
}

export default DateInput;