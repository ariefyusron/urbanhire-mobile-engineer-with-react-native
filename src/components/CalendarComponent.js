import React, { Component } from 'react';
import { LocaleConfig, Calendar, CalendarList } from 'react-native-calendars';
import { connect } from 'react-redux';

import { getDateNow, setModalVisible } from '../redux/actions/home';

LocaleConfig.locales['fr'] = {
  monthNames: ['January','February','March','April','May','June','July','August','September','October','November','December'],
  monthNamesShort: ['Jan','Feb','Mar','Apr','May','Jun','Jul.','Aug','Sep','Oct','Nov','Dec'],
  dayNames: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
  dayNamesShort: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
};

LocaleConfig.defaultLocale = 'fr';

class CalendarComponent extends Component {

  getFormatDateNow(){
    let date = new Date()
    date = date.toISOString().split('T')
    return date[0]
  }

  componentDidMount() {
    this.props.dispatch(getDateNow(this.getFormatDateNow()))
  }

  handleDayPress(day){
    this.props.dispatch(setModalVisible(true,'listEvents',day))
  }

  render() {
    return (
      <CalendarList
        onDayPress={(day) => this.handleDayPress(day.dateString)}
        markedDates={Object.assign(this.props.home.markedDate, this.props.event.markedDate)}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  home: state.home,
  event: state.event
})

export default connect(mapStateToProps)(CalendarComponent);