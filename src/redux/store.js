import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import { persistStore } from 'redux-persist';

import reducers from './reducers';

const store = createStore(
  reducers,
  applyMiddleware(
    logger
  )
);

const persistor = persistStore(store)

export {
  store,
  persistor
};