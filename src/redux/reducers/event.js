const initialState = {
  markedDate: {},
  events: []
}

const eventReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'ADD_EVENT':
      return {...state, events: [...state.events, action.payload], markedDate: {...state.markedDate, [action.payload.date]: {marked: true}}}
    default:
      return state;
  }
};

export default eventReducer;