import { AsyncStorage } from 'react-native';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { persistReducer } from 'redux-persist';

import homeReducer from './home';
import eventReducer from './event';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, eventReducer);

const reducers = combineReducers({
  form: formReducer,
  home: homeReducer,
  event: persistedReducer
});

export default reducers;